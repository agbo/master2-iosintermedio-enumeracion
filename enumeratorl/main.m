//
//  main.m
//  enumeratorl
//
//  Created by Fernando Rodríguez Romero on 11/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // Array
        NSArray *maybeNumbers = @[@"cinco", @1, @3.5, @YES, @(1 + 5.6)];
        
        [maybeNumbers enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSLog(@"Object %@ is at index: %lu", obj, (unsigned long)idx);
            if (idx >= 2){
                *stop = YES;
            }
        }];
        
        
        
        // Dictionary
        NSDictionary *dict = @{@"cinco" : @"five", @"uno" : @"one"};
        [dict enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            NSLog(@"%@ : %@", key, obj);
        }];
        
        
        
    }
    return 0;
}
